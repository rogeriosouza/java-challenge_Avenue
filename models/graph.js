var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GraphSchema = new Schema({
  name: {type: String, required: true, max: 100},
  price: {type: Number, required: true},
});


// Export the model
module.exports = mongoose.model('Graph', GraphSchema);