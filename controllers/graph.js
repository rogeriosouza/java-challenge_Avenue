var Graph = require('../models/graph');
var fs = require("fs");

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');  
};

exports.graph_list = function (req, res) {

    fs.readFile( __dirname + "/../" + "graph.json", 'utf8', function (err, data) {
        console.log( data );
        res.end( data );
    });
};

exports.routes = function (req, res) {

 // First read existing 
    fs.readFile( __dirname + "/../" + "routes.json", 'utf8', function (err, data) {
        data = JSON.parse( data );
        data["routes"] = user["routes"];
        console.log( data );
        res.end( JSON.stringify(data));
    })
}
exports.graph_ById = function (req, res) {
   // First read existing 
   fs.readFile( __dirname + "/../" + "graph.json", 'utf8', function (err, data) {
    var graphs = JSON.parse( data );
    var graph = graphs["data" + req.params.id] 
    
    console.log( graph );
    res.end( JSON.stringify(graph));
 });

    // Graph.findById(req.params.id, function (err, graph) {
    //     if (err) return next(err);
    //     res.send(graph);
    // })
};

exports.distance_ById = function (req, res) {
    // First read existing 
    fs.readFile( __dirname + "/../" + "distance.json", 'utf8', function (err, data) {
     var distances = JSON.parse( data );
     var distances = distances["data" + req.params.id] 
     
     console.log( distances );
     res.end( JSON.stringify(distances));
  });
 
     // Graph.findById(req.params.id, function (err, graph) {
     //     if (err) return next(err);
     //     res.send(graph);
     // })
 };


exports.graph_update = function (req, res) {
    Graph.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, graph) {
        if (err) return next(err);
        res.send('Graph udpated.');
    });
};

exports.graph_delete = function (req, res) {

       // First read existing users.
   fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
    data = JSON.parse( data );
    delete data["user" + 2];
    
    console.log( data );
    res.end( JSON.stringify(data));
});
    // Graph.findByIdAndRemove(req.params.id, function (err) {
    //     if (err) return next(err);
    //     res.send('Deleted successfully!');
    // })
};