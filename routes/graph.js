var express = require('express');
var router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
var graph_controller = require('../controllers/graph');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', graph_controller.test);


router.get('/graph', graph_controller.graph_list);
router.get('/routes/1/from/A/to/C?maxStops=3', graph_controller.routes);
//router.get('/distance/1/from/A/to/c', graph_controller.distance_filter);


router.get('/:id', graph_controller.graph_ById);

router.get('/distance/:id', graph_controller.distance_ById);

//router.put('/:id/update', graph_controller.graph_update);

//router.delete('/:id/delete', graph_controller.graph_delete);


module.exports = router;