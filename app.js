// app.js

var express = require('express');
var bodyParser = require('body-parser');

var graph = require('./routes/graph'); // Imports routes for the graph
var app = express();


// // Set up mongoose connection
// var mongoose = require('mongoose');
// var dev_db_url = 'mongodb://someuser:abcd0000@ds123611.mlab.com:23619/collection';
// var mongoDB = process.env.MONGODB_URI || dev_db_url;
// mongoose.connect(mongoDB);
// mongoose.Promise = global.Promise;
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/challenge', graph);

var port = 3000;

var server = app.listen(port, () => {

    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port)
    console.log('Server is up and running on port numner ' + port);
});
